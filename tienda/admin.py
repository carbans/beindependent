from django.contrib import admin
from tienda.models import Producto, Cliente


class ProductoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'precio', 'cantidad', 'disponible')
    search_fields = ('nombre',)

class ClienteAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellidos', 'email', 'provincia', 'sexo')
    search_fields = ('nombre', 'apellidos', 'email', 'provincia', 'movil')

admin.site.register(Producto, ProductoAdmin)
admin.site.register(Cliente, ClienteAdmin)