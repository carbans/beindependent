from __future__ import unicode_literals

from django.db import models


class Producto(models.Model):
    nombre = models.CharField(max_length=200)
    precio = models.FloatField()
    cantidad = models.IntegerField()
    descripcion = models.TextField()
    foto = models.ImageField(upload_to='productos/%Y/%m/%d')
    ancho = models.FloatField()
    alto = models.FloatField()
    peso = models.FloatField()
    disponible = models.BooleanField()

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = "producto"
        verbose_name_plural = "productos"
        ordering = ["nombre"]


class Cliente(models.Model):
    nombre = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=200)
    email = models.EmailField()
    movil = models.IntegerField()
    nacimiento = models.DateField()
    calle = models.CharField(max_length=200)
    poblacion = models.CharField(max_length=200)
    provincia = models.CharField(max_length=200)
    cp = models.IntegerField()
    pais = models.CharField(max_length=200)
    sexo = models.CharField(max_length=1, choices=(('M', 'Mujer'), ('H', 'Hombre')))

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = "cliente"
        verbose_name_plural = "clientes"
        ordering = ["nombre"]